﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRecopie
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtRecopie = New System.Windows.Forms.TextBox()
        Me.lblRecopie = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtRecopie
        '
        Me.txtRecopie.Location = New System.Drawing.Point(121, 108)
        Me.txtRecopie.Name = "txtRecopie"
        Me.txtRecopie.Size = New System.Drawing.Size(205, 20)
        Me.txtRecopie.TabIndex = 0
        '
        'lblRecopie
        '
        Me.lblRecopie.AutoSize = True
        Me.lblRecopie.Location = New System.Drawing.Point(118, 189)
        Me.lblRecopie.Name = "lblRecopie"
        Me.lblRecopie.Size = New System.Drawing.Size(148, 13)
        Me.lblRecopie.TabIndex = 1
        Me.lblRecopie.Text = "Le texte se recopie au-dessus"
        '
        'frmRecopie
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(446, 295)
        Me.Controls.Add(Me.lblRecopie)
        Me.Controls.Add(Me.txtRecopie)
        Me.Name = "frmRecopie"
        Me.Text = "frmRecopie"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtRecopie As TextBox
    Friend WithEvents lblRecopie As Label
End Class
