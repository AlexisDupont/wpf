﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TD2_progobjet
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Cvs_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //Exécuté lors d'un click sur le canvas
            Point P= Mouse.GetPosition(Cvs);
            MessageBox.Show(P.X + ", " + P.Y);
        }
    }
}
